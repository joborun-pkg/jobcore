#!/usr/bin/bash
# JOBoRun		: Jwm OpenBox Obarun RUNit
# Maintainer	: Joe Bo Run <joborun@disroot.org>
# PkgSource		: url="https://git.disroot.org/joborun-pkg/jobcore/src/branch/main/$pkgname"
# Website		: https://pozol.eu
#-----------------------------------------| DESCRIPTION |---------------------------------------

pkgname=dmraid
pkgver=1.0.0.rc16.3
_pkgver=1.0.0.rc16-3  ## ver that source has is not allowed for pkgver
pkgrel=015
pkgdesc="Device mapper RAID interface"
url="https://people.redhat.com/~heinzm/sw/dmraid"
conflicts=('mkinitcpio<0.7')
depends=('device-mapper>=2.0.54')
source=($url/src/$pkgname-$_pkgver.tar.bz2
        dmraid_install
        dmraid_hook
        dmraid_tmpfiles
        dmraid-format-security.patch)
install=dmraid.install

# As I get a security format error on my build I am turning those flags off
# just as arch does on gcc and elsewhere
#

prepare() {
  cd "$pkgname/$_pkgver/$pkgname/"
  patch -Np1 -i ${srcdir}/dmraid-format-security.patch
}

build() {
  cd "$pkgname/$_pkgver/$pkgname/"
  ./configure \
	prefix=/usr \
	sbindir=/usr/bin \
	libdir=/usr/lib \
	mandir=/usr/share/man \
	includedir=/usr/include \
	--enable-led \
	--enable-intel_led
  make
}

package() {
  cd "$pkgname/$_pkgver/$pkgname/"

  make DESTDIR="$pkgdir" sbindir=/usr/bin prefix=/usr libdir=/usr/lib mandir=/usr/share/man includedir=/usr/include install

  install -D -m644 "$srcdir"/dmraid_install "$pkgdir"/usr/lib/initcpio/install/dmraid
  install -D -m644 "$srcdir"/dmraid_hook "$pkgdir"/usr/lib/initcpio/hooks/dmraid
  install -D -m644 "$srcdir"/dmraid_tmpfiles "$pkgdir"/usr/lib/tmpfiles.d/dmraid.conf

  # fix permissions
  chmod 644 "$pkgdir"/usr/include/dmraid/* "$pkgdir"/usr/lib/libdmraid.a
}

#---- arch license gpg-key & sha256sums ----

arch=(x86_64)

license=('GPL-2.0-only')

sha256sums=(93421bd169d71ff5e7d2db95b62b030bfa205a12010b6468dcdef80337d6fbd8  # dmraid-1.0.0.rc16-3.tar.bz2
	6e74f7e1d66fbe5fc41560dfebb327eb31eb3104fec09985e388c2fe252d6575  # dmraid_install
	e74cdc82b59160d079594ce082980a3ae3561b5f6ecbdfdd30b7497b76373a2b  # dmraid_hook
	f00fa4462d83553d2fb769159a019b4d4898634e52b4a9bf796fe52b3b97f0cc  # dmraid_tmpfiles
	7c91f82db42018b09cf19fb43d3e1f672cc0b926acc6c572b01c7724206da650) # dmraid-format-security.patch
 
##  af3db11d0e0f502854262277552dbeb00c677ff64af293307eccb88afee42567  dmraid-1.0.0.rc16.3-015-x86_64.pkg.tar.lz

