#!/usr/bin/bash
# JOBoRun		: Jwm OpenBox Obarun RUNit
# Maintainer	: Joe Bo Run <joborun@disroot.org>
# PkgSource		: url="https://git.disroot.org/joborun-pkg/jobcore/src/branch/main/$pkgname"
# Website		: https://pozol.eu
#-----------------------------------------| DESCRIPTION |---------------------------------------

pkgname=coreutils
pkgver=9.6
pkgrel=04
pkgdesc='The basic file, shell and text manipulation utilities of the GNU operating system'
url='https://www.gnu.org/software/coreutils/'
depends=('glibc' 'acl' 'attr' 'gmp' 'libcap' 'openssl')
makedepends=(git gperf wget gettext python)
#source=("https://ftp.gnu.org/gnu/$pkgname/$pkgname-$pkgver.tar.xz"{,.sig})
source=(git+https://git.savannah.gnu.org/git/coreutils.git?signed#tag=v${pkgver}
  git+https://git.savannah.gnu.org/git/gnulib.git
  $pkgname-9.6-fix-ls-crash.patch  # https://git.savannah.gnu.org/gitweb/?p=coreutils.git;a=patch;h=915004f403cb25fadb207ddfdbe6a2f43bd44fac
  $pkgname-9.6-cat-crash.patch) # https://git.savannah.gnu.org/gitweb/?p=coreutils.git;a=patch;h=7386c291be8e2de115f2e161886e872429edadd7

prepare() {
  cd $pkgname
  git submodule init
  git config submodule.gnulib.url ../gnulib
  git -c protocol.file.allow=always submodule update

  ./bootstrap
  # apply patch from the source array (should be a pacman feature)

  local src
  for src in "${source[@]}"; do
  src="${src%%::*}"
    src="${src##*/}"
    [[ $src = *.patch ]] || continue
    echo "Applying patch $src..."
    patch -Np1 < "../$src"
  done
}

build() {
  cd $pkgname
  ./configure \
      --prefix=/usr \
      --libexecdir=/usr/lib \
      --with-openssl \
      --enable-no-install-program=hostname,kill,uptime
  make
}

check() {
  cd $pkgname
  make check
}

package() {
  cd $pkgname
  make DESTDIR="$pkgdir" install
}

#---- arch license gpg-key & sha256sums ----

arch=(x86_64)

license=('GPL-3.0-or-later' 'GFDL-1.3-or-later')

validpgpkeys=('6C37DC12121A5006BC1DB804DF6FD971306037D9') # Pádraig Brady

b2sums=('8d8ee559af5401564314c87e6b2affb670d6de59546b23dab3fa5235d6d3c71f841f91dcb6daf9bf38db25ebc3c21db4f9a536568744fabe3d02bcf9430c90ca'
        'SKIP'
        'd365086f33ffd770c8f457348561ed4120919c84f1bd4126495cd339f85a1fbf99845e1b17032a4ea579a93986f45ad71add1cd997e6a43235852087eac1279d'
        'f2d56b36ed9689fc7b879eb2985ad6aaeb8f50e5c1e7ac3b1ae064aad91d06198ea021f98792e7fb50994aa25f37a72edfcae40c59371427d477399327e2a8f1')

sha256sums=(5de297fae20e6dbb7a487cb1aac23daa3f5c1afc7876c0a2653f1ae8706156e0  # coreutils 9.6
	SKIP # gnulib
	4a3899d8df99c3501f8a6f8babb522b61edf4c94ff41535fc3aa8249925dfff7  # coreutils-9.6-fix-ls-crash.patch
	a65a11d1414ea2e5fca428a98665a302e918db1bda43fff471427420ebb85043) # coreutils-9.6-cat-crash.patch

##  35a6ca16648cfb4d55d1fb6d7ab095ce9fa3f60b70602940b1d737ff0b17650d  coreutils-9.6-04-x86_64.pkg.tar.lz

