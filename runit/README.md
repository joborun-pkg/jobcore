## G.Pape finally caved in and incorporated all patches gathered and created by Clan Z. Liu to create version 2.2.0

## runit patched and rebuilt with Clan Z. Liu patches for gcc 14+

### This is important to those running on runit

As mentioned before since arch moved to gcc 14 runit failed to build. All previous building warnings 
turned to errors. https://github.com/clan/runit a gentoo maintainer of runit, trying to keep it alive 
when gentoo was about to drop it (source based distro, no binaries, everything has to build at all 
times - what joborun was expected to be as well) developed a series of patches for every error AND 
every warning that gcc 14 and 15 were causing. Some patches from the past that came from debian, void, 
and artix were either redundant, conflicting, or causing errors themselves, except for one for svlogd 
which we kept as it seems good. 

Together with a recent clean-up of runit-rc this makes runit more solid but please pay attention to 
your booting for any “fail” tags and /var/log/dmesg.log and report.

On our few different systems it is all flags green (except for udevd seems to be taking a little 
longer than expected in some old machines but no problems).

#joborun

ref: https://diaspora-fr.org/posts/e2656b705980013d9f9c0025900e4586

