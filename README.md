# jobcore

joborun's core repository 

With the exception of systemd and related libraries and tools all other Arch core 
packages are rebuilt in a leaner environment as possible while trying to maintain the most 
compatibility with ARCH so all other packages will work correctly.

Joborun team

The repository should go on top of all other repositories and look like this

/etc/pacman.conf

    [jobcore]
    #Server = file:///var/cache/jobcore/
    Include = /etc/pacman.d/mirrorlist-jobo

where /etc/pacman.d/mirrorlist-jobo should look something like this:

    Server = http://downloads.sourceforge.net/joborun/r

You can switch between local and sf after building what you are interested in.  

Clone the repository into /src/pkg and [[build|https://git.disroot.org/joborun/web/src/branch/main/howto.md]] the package of choice:

    cd /src/pkg
    git clone https://git.disroot.org/joborun-pkg/jobcore.git jobcore
    cd jobcore

