This is the base package for the minimal joborun installation.

Make sure you choose a kernel and bootloader before you attempt 
your first boot and also run jobo-setup as root to ensure proper 
minimal configuration.

Recommended kernel built by us is linux-lts (5.10) or linux (5.15)
We have more ranging from 5.4 to 6.6 
________

For a minimal chroot pkg building environment install jobbot1 & jobbot2.
This is all you need to start building, see the wiki for details.
________

What you initially get is a console non graphic system, but we 
have a setup script in the tarball's /usr/local/bin/ directory 
that you can choose between an openbox or Jwm bundle, if this is
what you like.  Of course you are free to install whatever 
environment you like from Obarun, Arch, OUR and AUR repositories.  
It is just that we are not willing to support and assist in 
anything beyond the minimalistic installation of a window manager 
and this without a Display Manager and its complications.
________

As other distros condition their users with a ton of packages and 
graphic eye candy we will try to de-condition our users by showing 
an alternative route of doing what you really want to do with the 
least possible complications.  For example, if the only reason you 
use the system is to use something like an office-suite to create 
and publish documents, all you may need is something like vtwm, 
where you can spread out a huge area of documents and data, and 
something like Libre-Office.  

You can boot into a graphical system requiring 120-150MB of Ram 
or 35MB in console, and get to work, without the overhead of a 
login-manager, a display-manager, and a huge desktop-setup.

get in touch:  joborun@disroot.org  or https://diaspora-fr.org/tags/joborun

We are always open to constructive criticism, suggestions, 
recommendations, complaints,... We are here to discuss, not just 
one on one, but collectively, all that we have an interest in 
improving this system.  We perceive joborun as an alternative 
initial proposal containing specific guidelines, values and 
principles.  Where it will go and how it will evolve will depend 
on the participating community, contributing, proposing, 
suggesting, arguing, debating, etc.  When we collectivel get
convinced of a change the change will take place according to 
our new agreement.

Simple?  Let us hope it is.

------ 

Feb 23 2025

To avoid overwriting or conflicting with an existing
init system, such as s6, dinit, openrc etc. runit in joborun
is structured to coexist.

To make runit your default init without replacing your default
init system you must mv (rename) your existing init to a different
file (ec mv /usr/bin/init /usr/bin/xyz-init) and to still be
able to boot that one you must add init=/usr/bin/xyz-init into
the linux command line in the boot loader config.
If it is a link like runit uses just replace the link by the
following routine
  cd /usr/bin
  ln -sf runit-init init

The pkg comes with nearly the same without the -f option so
it will fail to replace an existing link or file
The 4 powerfunctions for runit (poweroff reboot halt shutdown) will
be moved in a non-default path again and there will be used throug
scripts in /usr/local/bin/ (poweroff reboot halt shutdown) that
you can edit accordingly

