
## As of February 4th 2025 - latest revision 0.7

(remove db from jobbot1 - must have been an old perl dependency if it is still installed)

This is the base package for the minimal jobbot installation.
This does not produce a bootable system alone, it is the minimum set number of packages to
have the minimum joborun building environment.  No kernel, no init, no eudev/udev (just libeudev).
The only unnecessary additions are cower (an AUR helper maintained currently by Eric Vidal of
Obarun) package-query (a very light program that helps searching AUR with similar output to yay,
and those are now part of jobbot2 which gets removed before building and before the additional
make dependencies (deps) are installed for each build.

1  pacman -S jobbot1 jobbot2
2  depS --> pacman -Rnsu jobbot2 && pacman -S $(cat deps) 
3  depR --> pacman -Rnsu $(cat deps) && pacman -S jobbot2 && ckchroot
4  you are back to 1
5  pacman -Qe should only show jobbot1 and jobbot2 

    Add the following shortcut to your .zshrc and use it to search for an AUR package in similar
    fashion to yay
    alias yay='f(){package-query -AQSs --rsort w --show-size $@;unset -f f;};f'  
    save, restart the zsh shell and run "yay aur helper" as a test

Before and after you build a package you should run ckchroot and expect no output with + in front of pkgname
+pkgname means you have extra pkgs installed, -pkgname shows pkgs missing.
There shouldn't be much missing if the system is to hold its integrity, even in chroot

When in doubt reinstall jobbot1 and 2 metapagkage.  If the chroot (or pacman within it) is broken, don't delete 
your work and abandon it.  Exit the chroot and issue " % sudo pacman -Sy jobbot1 jobbot2 -r /mnt " , where /mnt 
can be changed with the mountpoint of your builder's partition/container/tmpfilesys.  Enter the chroot again as in " % sudo 
arch-chroot /mnt zsh " and everything should be operational again.

________

It is crucial to maintain this base of installed packages because all pkg directories in
/src/pkg/jobcore and /src/pkg/jobextra contain a file called deps.  deps for each package
are the exact packages you need more than what you have in jobbase.  You install deps list 
to build, you uninstall deps list when you finish, and you always end up with jobbot.

To CLONE the source for joborun do the following or use the script in /src/pkg/

% -->
cd /src/pkg
sudo pacman -S git
git clone https://git.disroot.org/joborun-pkg/jobcore.git jobcore
git clone https://git.disroot.org/joborun-pkg/jobextra.git jobextra
git clone https://git.disroot.org/joborun-pkg/jobcomm.git jobcomm
sudo pacman -Rnsu git
sudo pacman -S jobbot1 jobbot2
ckchroot
% <--

you should end up with the three repositories containing the source of all
packages we produce and publish.  Read the wiki for building details. (http://pozol.eu --> joborun)
________

#### delete the empty spaces from the addresses below  ########
	joborun @ disroot . org       reddit . com / r / joborun (attempted to be abandoned 
            now moved to https:// diaspora-fr. org/tags/joborun as a forum for discussion (remember
	to add #joborun tag in your message if starting a new thread, so it can be noticed
	by members of the joborun community)
#### delete the empty spaces from the above addresses  ########

We are always open to constructive criticism, suggestions, recommendations, complaints,... 
We are here to discuss, not just one on one, but collectively, all that we have an interest in improving 
this system.  This is not my system, it is our system.  Please adjust your mode of thinking
accordingly.  Rational arguments only allowed and the stronger ones always produce a common decision
aceeptable by all.



## ATTENTION: If you are creating a build chroot/container/tmpfiles system by just installing jobbot1 jobbot2
 (that is not using the jobbot.tar.xz image) make sure you set /etc/hostname /etc/localtime /etc/timezone and 
 set locales by editing /etc/locale.gen and running locale-gen after editing it.  Also make sure a user such as
 our default "make" is created in the system and has a /home/make so GnuPG keys can be stored to verify validity
 of sources.

