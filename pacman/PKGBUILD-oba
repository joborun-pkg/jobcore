# Obarun        : 66 init/supervisor
# Maintainer    : Eric Vidal <eric@obarun.org>
# Maintainer    : Jean-Michel T.Dydak <jean-michel@obarun.org>
# Maintainer    : YianIris <yianiris@disroot.org>
# PkgSource     : url="https://git.obarun.org/pkg/obcore/pacman"
#----------------
# Website       : https://www.archlinux.org/pacman
#-----------------------------------------------------------------------------------------------
#-----------------------------------------| DESCRIPTION |---------------------------------------

pkgname=pacman
pkgver=6.0.2
pkgrel=7
pkgdesc="A library-based package manager with dependency support"

url="https://www.archlinux.org/pacman/"

track=
target="${pkgname}-${pkgver}.tar.xz"

source=("https://sources.archlinux.org/other/pacman/${target}"
    pacman-always-create-directories-from-debugedit.patch::https://gitlab.archlinux.org/pacman/pacman/-/commit/efd0c24c07b86be014a4edb5a8ece021b87e3900.patch
    pacman-always-create-directories-from-debugedit-fixup.patch::https://gitlab.archlinux.org/pacman/pacman/-/commit/86981383a2f4380bda26311831be94cdc743649b.patch
    pacman-fix-unique-source-paths.patch::https://gitlab.archlinux.org/pacman/pacman/-/commit/478af273dfe24ded197ec54ae977ddc3719d74a0.patch
    pacman-strip-include-o-files-similar-to-kernel-modules.patch::https://gitlab.archlinux.org/pacman/pacman/-/commit/de11824527ec4e2561e161ac40a5714ec943543c.patch
    'pacman.conf'
    'makepkg.conf'
    '0001-git_clone_with_ssh.patch'
    'sync_first_option.patch'
    )

#-------------------------------------| BUILD CONFIGURATION |-----------------------------------

options=(
    'strip'
    'debug')

makedepends=(
    'meson'
    'asciidoc'
    'doxygen')

checkdepends=(
    'python'
    'fakechroot')

#--------------------------------------| BUILD PREPARATION |------------------------------------

prepare() {
    cd "${pkgname}-${pkgver}"

    # we backport way too often in pacman
    # lets at least make it more convenient
    local src
    for src in "${source[@]}"; do
        src="${src%%::*}"
        src="${src##*/}"
        [[ $src = *.patch ]] || continue
        msg2 "Applying patch $src..."
        patch -Np1 < "../$src"
    done
    #patch -p1 -i ../0001-git_clone_with_ssh.patch
    #patch -Np1 -i ../sync_first_option.patch
}

#--------------------------------------------| BUILD |------------------------------------------

build() {
    cd "${pkgname}-${pkgver}"

    meson --prefix=/usr \
        --buildtype=plain \
        -Ddoc=enabled \
        -Ddoxygen=enabled \
        -Dscriptlet-shell=/usr/bin/bash \
        -Dldconfig=/usr/bin/ldconfig \
        build

    meson compile -C build
}

#--------------------------------------------| CHECK |------------------------------------------

check() {
    cd "${pkgname}-${pkgver}"

    meson test -C build
}

#-------------------------------------------| PACKAGE |-----------------------------------------

package() {
    cd "${pkgname}-${pkgver}"

    DESTDIR="$pkgdir" meson install -C build

    ## install Arch specific stuff
    install -dm755 "${pkgdir}/etc"
    install -m644 "${srcdir}/pacman.conf" "${pkgdir}/etc"
    install -m644 "${srcdir}/makepkg.conf" "${pkgdir}/etc"
}

#------------------------------------| INSTALL CONFIGURATION |----------------------------------

arch=('x86_64')

groups=(
    'base'
    'base-devel')

backup=(
    'etc/pacman.conf'
    'etc/makepkg.conf')

depends=(
    'bash'
    'glibc'
    'libarchive'
    'curl'
    'gpgme'
    'pacman-mirrorlist'
    'gettext'
    'gawk'
    'coreutils'
    'gnupg'
    'grep'
    'archlinux-keyring'
    'obarun-keyring')

optdepends=(
    'perl-locale-gettext: translation support in makepkg-template'
    )

provides=(
    'libalpm.so')

#-------------------------------------| SECURITY AND LICENCE |----------------------------------

license=('GPL')

validpgpkeys=('6645B0A8C7005E78DB1D7864F99FFE0FEAE999BD'  # Allan McRae <allan@archlinux.org>
              'B8151B117037781095514CA7BBDFFC92306B1121') # Andrew Gregory (pacman) <andrew@archlinux.org>

sha512sums=('9d76fb58c3a50e89a4b92b1f9e3bfdecca3f69e05022ea88fbd34f9df540c4fc688ad4f8b27e77eedb791aa682c27037abe65c789c6d9ee393bae5b620c3df13'
            'c609312c74bfbc75edd969cc60e441e3467a28f064f316c8993efce2b3d99aae87df22956a6e0e61ed1f151af238578216b4e757bf317df5136944c03d8cc137'
            '71a98c0754e3fd7fb471fd75e09ab4560e2117b178ea07a6a90aaa10f25fba111a6aec095721d0e5155b27635bae117dd11a81c8222fd65f78fd5d14ff865580'
            '88e442d1ef2fc57939856a15714b67e6d6aa83d74a51e0d9cc48aab0204a44b9008fa219268da942af2ad221792bb704cab0c404c0c054e9c35b98bb2ba5f118'
            '9d6d5d017f5d1971a09f6767d1754e1a6e9a26617693101b52ef75cc375a6ca1023f211c21a21120d64ee004ad98737fa4f1033145ff511dc2e8d24b9b04f96c'
            '1b36ae6ab7d769e77889670f255a77e58b8c78231e443c6cb0a196af0fa259273ba683719d324ccad0a24c038a5e3e95292866f527cf2b3f0f5d4ef7ae93612d'
            'cc1e05ae9be659597794000175793891c9f3aa47851bc40bc54b3f1d70175cb06e4ec0139b6f03ec8e2081ddc56b401968b1c75fa51ce53d573e1edffa797661'
            '69a43ac711da341e99186e3c1ea1c86d939dbd4c9a9781376bdbf78d31cdd824eb20ee880839e8b2318885e8b43de76def7a385c89125e6d3bf9b9b8639bee94'
            'cc01b34de2deb2897acf446b6d7fe99eaec83612dca689d5ec20d7fd5fcddd6ca6876cc5d71cd621cc14d330bb83e0a72d320413b687a997b7ed441feb19435d')

