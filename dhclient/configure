`configure' configures DHCP 4.4.3-P1 to adapt to many kinds of systems.

Usage: src/dhcp-4.4.3-P1/configure [OPTION]... [VAR=VALUE]...

To assign environment variables (e.g., CC, CFLAGS...), specify them as
VAR=VALUE.  See below for descriptions of some of the useful variables.

Defaults for the options are specified in brackets.

Configuration:
  -h, --help              display this help and exit
      --help=short        display options specific to this package
      --help=recursive    display the short help of all the included packages
  -V, --version           display version information and exit
  -q, --quiet, --silent   do not print `checking ...' messages
      --cache-file=FILE   cache test results in FILE [disabled]
  -C, --config-cache      alias for `--cache-file=config.cache'
  -n, --no-create         do not create output files
      --srcdir=DIR        find the sources in DIR [configure dir or `..']

Installation directories:
  --prefix=PREFIX         install architecture-independent files in PREFIX
                          [/usr/local]
  --exec-prefix=EPREFIX   install architecture-dependent files in EPREFIX
                          [PREFIX]

By default, `make install' will install all the files in
`/usr/local/bin', `/usr/local/lib' etc.  You can specify
an installation prefix other than `/usr/local' using `--prefix',
for instance `--prefix=$HOME'.

For better control, use the options below.

Fine tuning of the installation directories:
  --bindir=DIR            user executables [EPREFIX/bin]
  --sbindir=DIR           system admin executables [EPREFIX/sbin]
  --libexecdir=DIR        program executables [EPREFIX/libexec]
  --sysconfdir=DIR        read-only single-machine data [PREFIX/etc]
  --sharedstatedir=DIR    modifiable architecture-independent data [PREFIX/com]
  --localstatedir=DIR     modifiable single-machine data [PREFIX/var]
  --runstatedir=DIR       modifiable per-process data [LOCALSTATEDIR/run]
  --libdir=DIR            object code libraries [EPREFIX/lib]
  --includedir=DIR        C header files [PREFIX/include]
  --oldincludedir=DIR     C header files for non-gcc [/usr/include]
  --datarootdir=DIR       read-only arch.-independent data root [PREFIX/share]
  --datadir=DIR           read-only architecture-independent data [DATAROOTDIR]
  --infodir=DIR           info documentation [DATAROOTDIR/info]
  --localedir=DIR         locale-dependent data [DATAROOTDIR/locale]
  --mandir=DIR            man documentation [DATAROOTDIR/man]
  --docdir=DIR            documentation root [DATAROOTDIR/doc/dhcp]
  --htmldir=DIR           html documentation [DOCDIR]
  --dvidir=DIR            dvi documentation [DOCDIR]
  --pdfdir=DIR            pdf documentation [DOCDIR]
  --psdir=DIR             ps documentation [DOCDIR]

Program names:
  --program-prefix=PREFIX            prepend PREFIX to installed program names
  --program-suffix=SUFFIX            append SUFFIX to installed program names
  --program-transform-name=PROGRAM   run sed PROGRAM on installed program names

System types:
  --build=BUILD     configure for building on BUILD [guessed]
  --host=HOST       cross-compile to build programs to run on HOST [BUILD]

Optional Features:
  --disable-option-checking  ignore unrecognized --enable/--with options
  --disable-FEATURE       do not include FEATURE (same as --enable-FEATURE=no)
  --enable-FEATURE[=ARG]  include FEATURE [ARG=yes]
  --enable-silent-rules   less verbose build output (undo: "make V=1")
  --disable-silent-rules  verbose build output (undo: "make V=0")
  --enable-maintainer-mode
                          enable make rules and dependencies not useful (and
                          sometimes confusing) to the casual installer
  --enable-dependency-tracking
                          do not reject slow dependency extractors
  --disable-dependency-tracking
                          speeds up one-time build
  --enable-debug          create a debug-only version of the software (default
                          is no).
  --enable-failover       enable support for failover (default is yes)
  --enable-execute        enable support for execute() in config (default is
                          yes)
  --enable-tracing        enable support for server activity tracing (default
                          is yes)
  --enable-delayed-ack    queues multiple DHCPACK replies (default is yes)
  --enable-dhcpv6         enable support for DHCPv6 (default is yes)
  --enable-dhcpv4o6       enable support for DHCPv4-over-DHCPv6 (default is
                          no)
  --enable-relay-port     enable support for relay port (default is no)
  --enable-paranoia       enable support for chroot/setuid (default is no)
  --enable-early-chroot   enable chrooting prior to configuration (default is
                          no)
  --enable-ipv4-pktinfo   enable use of pktinfo on IPv4 sockets (default is
                          no)
  --enable-use-sockets    use the standard BSD socket API (default is no)
  --enable-log-pid        Include PIDs in syslog messages (default is no).
  --enable-binary-leases  enable support for binary insertion of leases
                          (default is no)
  --enable-kqueue         use BSD kqueue (default is no)
  --enable-epoll          use Linux epoll (default is no)
  --enable-devpoll        use /dev/poll (default is no)
  --enable-libtool        use GNU libtool for dynamic shared libraries
                          (default is no).
  --enable-bind-install   install bind includes and libraries (default is no).

Optional Packages:
  --with-PACKAGE[=ARG]    use PACKAGE [ARG=yes]
  --without-PACKAGE       do not use PACKAGE (same as --with-PACKAGE=no)
  --with-atf=PATH         specify location where atf was installed
  --with-srv-conf-file=PATH
                          Default file containing dhcpd configuration (default
                          is typically /etc/dhcpd.conf)
  --with-srv-lease-file=PATH
                          File for dhcpd leases (default is
                          LOCALSTATEDIR/db/dhcpd.leases)
  --with-srv6-lease-file=PATH
                          File for dhcpd6 leases (default is
                          LOCALSTATEDIR/db/dhcpd6.leases)
  --with-cli-lease-file=PATH
                          File for dhclient leases (default is
                          LOCALSTATEDIR/db/dhclient.leases)
  --with-cli6-lease-file=PATH
                          File for dhclient6 leases (default is
                          LOCALSTATEDIR/db/dhclient6.leases)
  --with-srv-pid-file=PATH
                          File for dhcpd process information (default is
                          LOCALSTATEDIR/run/dhcpd.pid)
  --with-srv6-pid-file=PATH
                          File for dhcpd6 process information (default is
                          LOCALSTATEDIR/run/dhcpd6.pid)
  --with-cli-pid-file=PATH
                          File for dhclient process information (default is
                          LOCALSTATEDIR/run/dhclient.pid)
  --with-cli6-pid-file=PATH
                          File for dhclient6 process information (default is
                          LOCALSTATEDIR/run/dhclient6.pid)
  --with-relay-pid-file=PATH
                          File for dhcrelay process information (default is
                          LOCALSTATEDIR/run/dhcrelay.pid)
  --with-relay6-pid-file=PATH
                          File for dhcrelay6 process information (default is
                          LOCALSTATEDIR/run/dhcrelay6.pid)
  --with-randomdev=PATH   Path for random device (default is /dev/random)
  --with-bind-extra-config
                          configure bind librairies with some extra options
                          (default is none)
  --with-libbind=PATH     bind includes and libraries are in PATH
  --with-ldap             enable OpenLDAP support in dhcpd (default is no)
  --with-ldapcrypto       enable OpenLDAP crypto support in dhcpd (default is
                          no)
  --with-ldap-gssapi      enable krb5/gssapi authentication for OpenLDAP in
                          dhcpd (default is no)
  --with-ldapcasa         enable LDAP CASA auth support in dhcpd (default is
                          no)

Some influential environment variables:
  CC          C compiler command
  CFLAGS      C compiler flags
  LDFLAGS     linker flags, e.g. -L<lib dir> if you have libraries in a
              nonstandard directory <lib dir>
  LIBS        libraries to pass to the linker, e.g. -l<library>
  CPPFLAGS    (Objective) C/C++ preprocessor flags, e.g. -I<include dir> if
              you have headers in a nonstandard directory <include dir>

Use these variables to override the choices made by `configure' or to help
it to find libraries and programs with nonstandard names/locations.

Report bugs to <dhcp-users@isc.org>.
