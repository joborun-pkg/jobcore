##  We 1st moved from OSDN because it is falling apart to 
##  SourceForge, which does not allow pkg names with a : in them
##  causing us further headaches, which we temporarily fixed
##  by adding all 37 affected pkgs to a tarball and having you
##  run a script to download them and place them in cache.
##
##  Finally we found a solution, as pacman is much smarter
##  than we expected, disregarding the name of the pkg and
##  reading pkg name epoch version and release from inside
##  the pkg.  So we can call a pkg kookoo.not-here and 
##  install it and it will register as its proper name and
##  version.   So we did exactly this, rename pkgs to fit
##  SourceForge Microsoftish servers and rerun the pacman
##  databases with the new names.
##  
##  In case something goes wrong in the future with SF mirrors
##  revert manually in your pacman.conf to this disroot
##  server holding just the databases, pacman, and jobo-mirror
##  pkgs.
##  
#   pacman database only, no packages to download

Server = https://git.disroot.org/joborun-pkg/repos/raw/branch/main/

#   osdn mirrors once they become available for uploads again
#   DOWN since July 9th 2023

#Server = https://ftp.iij.ad.jp/pub/osdn.jp/storage/g/j/jo/joborun/repos/
#Server = https://free.nchc.org.tw/osdn/storage/g/j/jo/joborun/repos/
#Server = https://osdn.net/projects/joborun/storage/repos/

