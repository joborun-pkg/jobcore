#!/usr/bin/sh
echo "this script downloads the current group of pkgs that sourceforge"
echo "is unable to serve due to the : colon in the epoch characteristic."
echo "it explodes the tarball in /tmp/sf01 then moves the packages to"
echo "your standard pacman cache at /var/cache/pacman/pkg which fools"
echo "pacman to not attempt a download from the mirror as the pkgs are"
echo "already in the cache."
echo "The benefit to all this is when you try to install one of them"
echo "you will not get a 404 error from the mirror. A dirty hack but"
echo "till we find a better solution and mirrors it will have to do."
  mkdir -p /tmp/sf01
  cd /tmp/sf01
  if [ ! -f /tmp/sf01/non-sf-pkgs.tar.xz ];
  then
    echo "tar-ball not found, downloading now!" \
      &&  wget http://downloads.sourceforge.net/joborun/r/non-sf-pkgs.tar.xz
  else
    echo "tar-ball already here, exploding and moving pkgs to cache"
    continue
  fi
  echo "checking sums, the two lines must be identical, if not remove the tarball and restart"
  echo "5d620b833b9dca4b6670723a8061cc57d14c4d3cb3760a9798a6bd691629bdb5  non-sf-pkgs.tar.xz"
  sha256sum non-sf-pkgs.tar.xz
  echo "enter to continue IF 2 lines identical"
  read
  tar -xf non-sf-pkgs.tar.xz 
  mv -f *pkg.tar.*z /var/cache/pacman/pkg/ 
  pwd
  ls -lh /tmp/sf01   
  echo "now you can resume with upgrades as normal % pacman -Suy "
  echo "We apologize for the inconvenience but this is all we can do for now"
