#!/usr/bin/bash
# JOBoRun		: Jwm OpenBox Obarun RUNit
# Maintainer	: Joe Bo Run <joborun@disroot.org>
# PkgSource		: url="https://git.disroot.org/joborun-pkg/jobcore/src/branch/main/$pkgname"
# Website		: https://pozol.eu
#-----------------------------------------| DESCRIPTION |---------------------------------------

pkgbase=util-linux
pkgname=(util-linux util-linux-libs)
pkgver=2.40.4
pkgrel=01
pkgdesc='Miscellaneous system utilities for Linux w/o systemd'
url='https://github.com/util-linux/util-linux'
#url='https://github.com/util-linux/util-linux'
makedepends=('asciidoctor'
             'bash-completion'
             'git'
             'libcap-ng'
             'libxcrypt'
             'meson'
             'python'
             'sqlite')
#options=('debug' 'strip')
options=('strip')
# source=("https://www.kernel.org/pub/linux/utils/util-linux/v${_pkgmajor}/${pkgbase}-${_realver}.tar."{xz,sign}
# source=("git+https://github.com/karelzak/util-linux#tag=${_tag}?signed"
# yet again in a month three sources for the same software ... lead by a man with a redhat account.
# need we say more for the puppets in kernel.org?
source=("git+https://github.com/util-linux/util-linux#tag=v${pkgver/rc/-rc}?signed"
        $pkgbase-BSD-2-Clause.txt::https://raw.githubusercontent.com/Cyan4973/xxHash/f035303b8a86c1db9be70cbb638678ef6ef4cb2d/LICENSE
        pam-{login,common,remote,runuser,su}
        'util-linux.sysusers'
        '60-rfkill.rules')

_backports=(
#  # build-sys: release++ (v2.40.4)
#  "v${pkgver}..dbcc687f6ab1568982cdf3fe391c0beb818b7e28"
)

_reverts=(
)

prepare() {
  cd "${pkgbase}"

  local _c _l
  for _c in "${_backports[@]}"; do
    if [[ "${_c}" == *..* ]]; then _l='--reverse'; else _l='--max-count=1'; fi
    git log --oneline "${_l}" "${_c}"
    git cherry-pick --mainline 1 --no-commit "${_c}"
  done
  for _c in "${_reverts[@]}"; do
    if [[ "${_c}" == *..* ]]; then _l='--reverse'; else _l='--max-count=1'; fi
    git log --oneline "${_l}" "${_c}"
    git revert --mainline 1 --no-commit "${_c}"
  done

#  # fast-forward to current master
#  git merge master

  # do not mark dirty
  sed -i '/dirty=/c dirty=' tools/git-version-gen
}

build() {
local _meson_options=(
  -Dfs-search-path=/usr/bin:/usr/local/bin

  -Dlibuser=disabled
    -Dlibutempter=disabled
  -Dncurses=disabled
  -Dncursesw=enabled
  -Deconf=disabled
  -Dsystemd=disabled
  -Dbuild-chfn-chsh=enabled
  -Dbuild-line=disabled
  -Dbuild-mesg=enabled
  -Dbuild-newgrp=enabled
  -Dbuild-vipw=enabled
  -Dbuild-write=enabled
  -Dbuild-plymouth-support=disabled
  )

  arch-meson "${pkgbase}" build "${_meson_options[@]}"

  meson compile -C build
}

package_util-linux() {
  conflicts=('rfkill' 'hardlink')
  provides=('rfkill' 'hardlink')
  replaces=('rfkill' 'hardlink')
  depends=("util-linux-libs=${pkgver}"
           'coreutils'
           'file' 'libmagic.so'
           'glibc'
           'libcap-ng'
           'libxcrypt' 'libcrypt.so'
           'ncurses' 'libncursesw.so'
           'pam'
           'readline'
           'shadow'
           'libeudev'
           'zlib')
  optdepends=('words: default dictionary for look')
  backup=(etc/pam.d/chfn
          etc/pam.d/chsh
          etc/pam.d/login
          etc/pam.d/remote
          etc/pam.d/runuser
          etc/pam.d/runuser-l
          etc/pam.d/su
          etc/pam.d/su-l)

  _python_stdlib="$(python -c 'import sysconfig; print(sysconfig.get_paths()["stdlib"])')"

  DESTDIR="${pkgdir}" meson install -C build

  # remove static libraries
  rm "${pkgdir}"/usr/lib/lib*.a*

  # setuid chfn and chsh
  chmod 4755 "${pkgdir}"/usr/bin/{newgrp,ch{sh,fn}}

  # install PAM files for login-utils
  install -Dm0644 pam-common "${pkgdir}/etc/pam.d/chfn"
  install -m0644 pam-common "${pkgdir}/etc/pam.d/chsh"
  install -m0644 pam-login "${pkgdir}/etc/pam.d/login"
  install -m0644 pam-remote "${pkgdir}/etc/pam.d/remote"
  install -m0644 pam-runuser "${pkgdir}/etc/pam.d/runuser"
  install -m0644 pam-runuser "${pkgdir}/etc/pam.d/runuser-l"
  install -m0644 pam-su "${pkgdir}/etc/pam.d/su"
  install -m0644 pam-su "${pkgdir}/etc/pam.d/su-l"

  # TODO(dreisner): offer this upstream?
##  sed -i '/ListenStream/ aRuntimeDirectory=uuidd' "${pkgdir}/usr/lib/systemd/system/uuidd.socket"

  # runtime libs are shipped as part of util-linux-libs
  install -d -m0755 util-linux-libs/lib/
  mv "$pkgdir"/usr/lib/lib*.so* util-linux-libs/lib/
  mv "$pkgdir"/usr/lib/pkgconfig util-linux-libs/lib/pkgconfig
  mv "$pkgdir"/usr/include util-linux-libs/include
  mv "$pkgdir"/"${_python_stdlib}"/site-packages util-linux-libs/site-packages
  rmdir "$pkgdir"/"${_python_stdlib}"
  mv "$pkgdir"/usr/share/man/man3 util-linux-libs/man3

  ### install sysusers
  install -Dm0644 util-linux.sysusers \
    "${pkgdir}/usr/lib/sysusers.d/util-linux.conf"

  install -Dm0644 60-rfkill.rules \
    "${pkgdir}/usr/lib/udev/rules.d/60-rfkill.rules"

  install -vDm 644 $pkgbase/Documentation/licenses/COPYING.{BSD*,ISC} -t "$pkgdir/usr/share/licenses/$pkgname/"
  install -vDm 644 $pkgbase-BSD-2-Clause.txt -t "$pkgdir/usr/share/licenses/$pkgname/"
}

package_util-linux-libs() {
  pkgdesc="util-linux runtime libraries"
  depends=('glibc'
           'sqlite')
  provides=('libutil-linux' 'libblkid.so' 'libfdisk.so' 'libmount.so' 'libsmartcols.so' 'libuuid.so')
  conflicts=('libutil-linux')
  replaces=('libutil-linux')
  optdepends=('python: python bindings to libmount')

  install -d -m0755 "$pkgdir"/{"${_python_stdlib}",usr/share/man/}
  mv util-linux-libs/lib/* "$pkgdir"/usr/lib/
  mv util-linux-libs/include "$pkgdir"/usr/include
  mv util-linux-libs/site-packages "$pkgdir"/"${_python_stdlib}"/site-packages
  mv util-linux-libs/man3 "$pkgdir"/usr/share/man/man3

  install -vDm 644 $pkgbase/Documentation/licenses/COPYING.{BSD*,ISC} -t "$pkgdir/usr/share/licenses/$pkgname/"
  install -vDm 644 $pkgbase-BSD-2-Clause.txt -t "$pkgdir/usr/share/licenses/$pkgname/"
}

#---- arch license gpg-key & sha256sums ----

arch=(x86_64)

license=(
  'BSD-2-Clause'
  'BSD-3-Clause'
  'BSD-4-Clause-UC'
  'GPL-2.0-only'
  'GPL-2.0-or-later'
  'GPL-3.0-or-later'
  'ISC'
  'LGPL-2.1-or-later'
  'LicenseRef-PublicDomain'
)

validpgpkeys=('B0C64D14301CC6EFAEDF60E4E4B71D5EEC39C284')  # Karel Zak

sha256sums=(32b88bea337aa283e4a84a20eb70fe8df5db6c09cfd99bc133e2503e2e6b53d6  # util-linux
	6ffedbc0f7878612d2b23589f1ff2ab15633e1df7963a5d9fc750ec5500c7e7a  # util-linux-BSD-2-Clause.txt
	ee917d55042f78b8bb03f5467e5233e3e2ddc2fe01e302bc53b218003fe22275  # pam-login
	57e057758944f4557762c6def939410c04ca5803cbdd2bfa2153ce47ffe7a4af  # pam-common
	8bfbee453618ba44d60ba7fb00eced6c62edebfc592f2e75dede08e769ed8931  # pam-remote
	48d6fba767631e3dd3620cf02a71a74c5d65a525d4c4ce4b5a0b7d9f41ebfea1  # pam-runuser
	3f54249ac2db44945d6d12ec728dcd0d69af0735787a8b078eacd2c67e38155b  # pam-su
	b3d8994c0ab0c50500ed249c90fb709555a369b910e5f3eca758a28d4e73e2d3  # util-linux.sysusers
	7423aaaa09fee7f47baa83df9ea6fef525ff9aec395c8cbd9fe848ceb2643f37) #  60-rfkill.rules

##  cef830c58710119fe74cc297530eb16461dd2aad5a52997539ce8e53c56d27a3  util-linux-2.40.4-01-x86_64.pkg.tar.lz
##  095213ee3f76326cd61ddd05b4381588b9a7498b5a23bea51f386649af2629cc  util-linux-libs-2.40.4-01-x86_64.pkg.tar.lz

##  b3d8994c0ab0c50500ed249c90fb709555a369b910e5f3eca758a28d4e73e2d3  # util-linux.sysusers
b3d8994c0ab0c50500ed249c90fb709555a369b910e5f3eca758a28d4e73e2d3  # util-linux.sysusers.1
