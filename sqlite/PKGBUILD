#!/usr/bin/bash
# JOBoRun		: Jwm OpenBox Obarun RUNit
# Maintainer	: Joe Bo Run <joborun@disroot.org>
# PkgSource		: url="https://git.disroot.org/joborun-pkg/jobcore/src/branch/main/$pkgname"
# Website		: https://pozol.eu
#-----------------------------------------| DESCRIPTION |---------------------------------------

pkgbase="sqlite"
pkgname=('sqlite' 'sqlite-tcl' 'sqlite-analyzer' 'lemon' 'sqlite-doc')
_srcver=3490100
_docver=${_srcver}
pkgver=3.49.1
pkgrel=01
pkgdesc="A C library that implements an SQL database engine"
url="https://www.sqlite.org/"
makedepends=('tcl=1:8.6.16' 'readline' 'zlib') # specific tcl ver because arch removed 9.0.1 from testing
#options=('!emptydirs' 'debug')  ## uncomment this to produce the debug pkg
options=('!emptydirs')
source=(https://www.sqlite.org/2025/sqlite-src-${_srcver}.zip
        https://www.sqlite.org/2025/sqlite-doc-${_docver}.zip
        sqlite-lemon-system-template.patch
        license.txt)

prepare() {
  cd sqlite-src-$_srcver

  # patch taken from Fedora
  # https://src.fedoraproject.org/rpms/sqlite/blob/master/f/sqlite.spec
  patch -Np1 -i ../sqlite-lemon-system-template.patch

  #autoreconf -vfi
}

build() {
  # this uses malloc_usable_size, which is incompatible with fortification level 3
  export CFLAGS="${CFLAGS/_FORTIFY_SOURCE=3/_FORTIFY_SOURCE=2}"
  export CXXFLAGS="${CXXFLAGS/_FORTIFY_SOURCE=3/_FORTIFY_SOURCE=2}"

  export CPPFLAGS="$CPPFLAGS \
	-DSQLITE_ENABLE_COLUMN_METADATA=1 \
	-DSQLITE_ENABLE_UNLOCK_NOTIFY \
	-DSQLITE_ENABLE_DBSTAT_VTAB=1 \
	-DSQLITE_ENABLE_FTS3_TOKENIZER=1 \
	-DSQLITE_ENABLE_FTS3_PARENTHESIS \
	-DSQLITE_SECURE_DELETE \
	-DSQLITE_ENABLE_STMTVTAB \
	-DSQLITE_ENABLE_STAT4 \
	-DSQLITE_MAX_VARIABLE_NUMBER=250000 \
	-DSQLITE_MAX_EXPR_DEPTH=10000 \
	-DSQLITE_ENABLE_MATH_FUNCTIONS"

  # build sqlite
  cd sqlite-src-$_srcver
  ./configure --prefix=/usr \
	--disable-static \
	--fts4 \
	--fts5 \
	--rtree \
    --soname=legacy
  sed -i -e 's/$(LDFLAGS.libsqlite3)/ -Wl,-O1,--as-needed \0/g' main.mk
  make
  # build additional tools - broken build: changeset rbu
  make dbdump dbhash dbtotxt index_usage scrub showdb showjournal showshm \
       showstat4 showwal sqldiff sqlite3_analyzer sqlite3_checker \
       sqlite3_expert sqlite3_rsync sqltclsh
}

package_sqlite() {

 pkgdesc="A C library that implements an SQL database engine"
 depends=('readline' 'zlib' 'glibc')
 provides=("sqlite3=$pkgver" 'libsqlite3.so')
 replaces=("sqlite3")

  cd sqlite-src-$_srcver
  make DESTDIR="${pkgdir}" install

  install -m755 dbdump dbhash dbtotxt index_usage scrub showdb showjournal showshm \
         showstat4 showwal sqldiff sqlite3_expert sqlite3_rsync "${pkgdir}"/usr/bin/

  # rename to avoid file conflicts
  mv "${pkgdir}"/usr/bin/scrub "${pkgdir}"/usr/bin/sqlite3_scrub

  # install manpage
  install -m755 -d "${pkgdir}"/usr/share/man/man1
  install -m644 sqlite3.1 "${pkgdir}"/usr/share/man/man1/

  # license
  install -D -m644 "${srcdir}"/license.txt "${pkgdir}"/usr/share/licenses/${pkgname}/license.txt

  # split out tcl extension
  mkdir "$srcdir"/tcl
  mv "$pkgdir"/usr/lib/tcl8.6/sqlite* "$srcdir"/tcl
#  With tcl 9.0 that we upgraded back in Jan 17 25 
#  this causes a build error, so we reverted to older tcl
#  
#  mv "$pkgdir"/usr/lib/tcl/sqlite* "$srcdir"/tcl

}

package_sqlite-tcl() {

 pkgdesc="sqlite Tcl Extension Architecture (TEA)"
 depends=('sqlite' 'tcl' 'glibc')
 provides=("sqlite3-tcl=$pkgver")
 replaces=("sqlite3-tcl")

  cd sqlite-src-$_srcver
  install -m755 -d "${pkgdir}"/usr/lib
  mv "$srcdir"/tcl/* "${pkgdir}"/usr/lib

  install -m755 -d "${pkgdir}"/usr/bin
  install -m755 sqlite3_checker sqltclsh "${pkgdir}"/usr/bin/

  # install manpage
  install -m755 -d "${pkgdir}"/usr/share/man/mann
  install -m644 "${srcdir}"/sqlite-src-$_srcver/autoconf/tea/doc/sqlite3.n "${pkgdir}"/usr/share/man/mann/

  # license
  install -D -m644 "${srcdir}"/license.txt "${pkgdir}"/usr/share/licenses/${pkgname}/license.txt
}

package_sqlite-analyzer() {

 pkgdesc="An analysis program for sqlite3 database files"
 depends=('sqlite' 'tcl' 'glibc')

  cd sqlite-src-$_srcver
  install -m755 -d "${pkgdir}"/usr/bin
  install -m755 sqlite3_analyzer "${pkgdir}"/usr/bin/

  # license
  install -D -m644 "${srcdir}"/license.txt "${pkgdir}"/usr/share/licenses/${pkgname}/license.txt
}

package_lemon() {

 # https://www.sqlite.org/lemon.html
 pkgdesc="A parser generator"
 depends=('glibc')

  cd sqlite-src-$_srcver
  # ELF file ('usr/bin/lemon') lacks FULL RELRO, check LDFLAGS. - no fix found so far
  install -Dm755 lemon ${pkgdir}/usr/bin/lemon
  install -Dm644 lempar.c ${pkgdir}/usr/share/lemon/lempar.c
  
  mkdir -p "${pkgdir}"/usr/share/doc/${pkgname}
  cp ../sqlite-doc-${_docver}/lemon.html  "${pkgdir}"/usr/share/doc/${pkgname}/
  # license
  install -D -m644 "${srcdir}"/license.txt "${pkgdir}"/usr/share/licenses/${pkgname}/license.txt
}

package_sqlite-doc() {

 pkgdesc="most of the static HTML files that comprise this website, including all of the SQL Syntax and the C/C++ interface specs and other miscellaneous documentation"
 #arch=('any') - not yet supported
 provides=("sqlite3-doc=$pkgver")
 replaces=("sqlite3-doc")

  cd sqlite-doc-${_docver}
  mkdir -p "${pkgdir}"/usr/share/doc/${pkgbase}
  cp -R *  "${pkgdir}"/usr/share/doc/${pkgbase}/

  # license
  install -D -m644 "${srcdir}"/license.txt "${pkgdir}"/usr/share/licenses/${pkgname}/license.txt
  
  rm "${pkgdir}"/usr/share/doc/${pkgbase}/lemon.html
}

#---- arch license gpg-key & sha256sums ----

arch=(x86_64)

license=('LicenseRef-Sqlite')

# upstream now switched to sha3sums - currently not supported by makepkg

b3sums=(f859249cca835b08ac6b957b0521aa0cf779d3683b3c828d941302c069cad777  # sqlite-src-3490000.zip
	f2494f3c7e575f2595817a5990ee964dd468590b3731caa88896bcb3ee21f55f  # sqlite-doc-3490000.zip
	eb01637216043bddd754a31211cd1ff9788d267e6e42a0b0553872ef5f65a6f9  # sqlite-lemon-system-template.patch
	294c2153066a0863230d17531afefcab280b77b7f8fa2bce8b9615c3c9ea653e) # license.txt

sha256sums=(4404d93cbce818b1b98ca7259d0ba9b45db76f2fdd9373e56f2d29b519f4d43b  # sqlite-src-3490100.zip 
	4581e3340d9d0d8ce03f10fb8ab1cea03cf49fed8198478c1abf5d383521f037  # sqlite-doc-3490100.zip
	55746d93b0df4b349c4aa4f09535746dac3530f9fd6de241c9f38e2c92e8ee97  # sqlite-lemon-system-template.patch
	4e57d9ac979f1c9872e69799c2597eeef4c6ce7224f3ede0bf9dc8d217b1e65d) # license.txt

##  86af4508861cf37d8209685fad27d547356cedba2f83cadc49aecc64dd21e7b7  lemon-3.49.1-01-x86_64.pkg.tar.lz
##  82a19f469cdcdf4049558c0b0279c38eb50b6f4ba88472bc660a5d8137cd3544  sqlite-3.49.1-01-x86_64.pkg.tar.lz
##  3e773ae1804c1487798e87e5bd31d5560644a19d907fa0cfff5efa4eeff44258  sqlite-analyzer-3.49.1-01-x86_64.pkg.tar.lz
##  7f8a41332714e4e6831ac8c3ff219566bda0713220433b9793441306b4acbd38  sqlite-doc-3.49.1-01-x86_64.pkg.tar.lz
##  b25df3dc97c2a5c00948850460ddf72a9c45f6ddbb6c1f46ddc4d54554303936  sqlite-tcl-3.49.1-01-x86_64.pkg.tar.lz

