
  This pkg was created with the sole purpose of blocking Obarun's boot@-66serv replaced by our own
  boot-66serv.  If you were to install the authentic Obarun boot@-66serv you would break the system,
  unless you are saying goodbye to runit and want it permanently removed, thus switching to the
  full Obarun setup.  The reason we rebuild boot-66serv as our own is so the conflicting files
  with runit/runit-rc do not conflict and can be located separately, while the 4 power functions
  (reboot, poweroff, halt, shutdown) can operate either one of the two systems (runit s6).

  
