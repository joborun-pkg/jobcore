# Maintainer: Eric Vidal <eric@obarun.org>
# based on the original https://projects.archlinux.org/svntogit/packages.git/tree/trunk?h=packages/filesystem
#                       Maintainer: Tom Gundersen <teg@jklm.no>


pkgname=filesystem
pkgver=2021.10.13
pkgrel=2
pkgdesc='Base Obarun filesystem'
arch=(x86_64)
license=('GPL')
url='https://www.obarun.org'
#install='filesystem.install'
depends=('iana-etc')
backup=('etc/crypttab' 'etc/fstab' 'etc/group' 'etc/gshadow' 'etc/host.conf'
        'etc/hosts' 'etc/issue' 'etc/ld.so.conf' 'etc/motd' 'etc/nsswitch.conf'
        'etc/passwd' 'etc/profile' 'etc/resolv.conf' 'etc/securetty'
        'etc/shadow' 'etc/shells')
source=('crypttab' 'fstab' 'group' 'gshadow' 'host.conf' 'hosts'
        'issue' 'ld.so.conf' 'locale.sh' 'motd' 'nsswitch.conf' 'os-release'
        'passwd' 'profile' 'resolv.conf' 'securetty' 'shadow' 'shells'
        'sysusers' 'tmpfiles' 'modprobe.d.usb-load-ehci-first')
md5sums=('0672783048ee312d2fc768402fdb3488'
         '693c97f2c9a519bb97a17008e92c2b74'
         '26a96329a5523e5c11c50be58e6758c8'
         'e411df94c344a6dbf6634f75ea59a5dc'
         '7c944ff2ac3b4fc5e3c2f3e9aa1ed802'
         '4c8ed1804d6ff26714530dd43341b7a1'
         '7a72f9ff248e284e2cedcd9c59542758'
         '5deb9f890a4d08a245e9752ede77271e'
         'a9cabd3090a240bafbb05ce443d991df'
         'd41d8cd98f00b204e9800998ecf8427e'
         '162cf609205e52a862fb1838d5df2be6'
         '8649a132297ee9fe3805b26a4d032f2d'
         'd49e3834aa82c71f71f058f23467bd6f'
         '448fe7cbcb566e8d318dc0db0c9cb710'
         '1e025adcb3b94399bc74858440453501'
         'f04bcb2803afc4dcb95670fe87343b4d'
         '1e867e07ad9a04f40fa9e5e4aa1f1624'
         '6e0cf9e2ec97a424dfb11d41d2ef3c91'
         'dff1b18db07570fd69f55f91f501ef0a'
         '0267a3a463f35eec8a31f40a720dfd86'
         'a8a962370cd0128465d514e6a1f74130')

package() {
    cd "$pkgdir"

    #
    # setup root filesystem
    #
    for d in boot dev etc home mnt usr var opt srv/http run; do
        install -d -m755 $d
    done
    install -d -m555 proc
    install -d -m555 sys
    install -d -m0750 root
    install -d -m1777 tmp
    # vsftpd won't run with write perms on /srv/ftp
    # ftp (uid 14/gid 11)
    install -d -m555 -g 11 srv/ftp

    # setup /etc and /usr/share/factory/etc
    install -d etc/{ld.so.conf.d,skel,profile.d} usr/share/factory/etc

    for f in fstab group host.conf hosts issue ld.so.conf motd nsswitch.conf passwd resolv.conf securetty shells profile; do
            install -m644 "$srcdir"/$f etc/
            install -m644 "$srcdir"/$f usr/share/factory/etc/
    done
    ln -s ../proc/self/mounts etc/mtab
    for f in gshadow shadow crypttab; do
        install -m600 "$srcdir"/$f etc/
        install -m600 "$srcdir"/$f usr/share/factory/etc/
    done
    install -D -m644 "$srcdir"/modprobe.d.usb-load-ehci-first usr/lib/modprobe.d/usb-load-ehci-first.conf

    #touch etc/obarun-release
    install -m644 "$srcdir"/locale.sh etc/profile.d/locale.sh
    install -Dm644 "$srcdir"/os-release usr/lib/os-release
    ln --symbolic --relative "$pkgdir"/usr/lib/os-release "$pkgdir"/etc/os-release

    # Cmake fail to install lib64 correctly without it
    touch etc/arch-release

    # setup /var
    for d in cache local opt log/old lib/misc empty; do
        install -d -m755 var/$d
    done
    install -d -m1777 var/{tmp,spool/mail}

    # allow setgid games to write scores
    install -d -m775 -g 50 var/games
    ln -s spool/mail var/mail
    ln -s ../run var/run
    ln -s ../run/lock var/lock

    #
    # setup /usr hierarchy
    #
    for d in bin include lib share/misc src; do
        install -d -m755 usr/$d
    done
    for d in {1..8}; do
        install -d -m755 usr/share/man/man$d
    done

    #
    # add lib symlinks
    #
    ln -s usr/lib lib
    ln -s usr/lib lib64
    ln -s lib usr/lib64


    #
    # add bin symlinks
    #
    ln -s usr/bin bin
    ln -s usr/bin sbin
    ln -s bin usr/sbin

    #
    # setup /usr/local hierarchy
    #
    for d in bin etc games include lib man sbin share src; do
        install -d -m755 usr/local/$d
    done
    ln -s ../man usr/local/share/man

    # setup wrapper file for systemd-sysusers
    install -D -m644 "$srcdir"/sysusers usr/lib/sysusers.d/obarun.conf

    # setup wrapper file for systemd-tmpfiles
    install -D -m644 "$srcdir"/tmpfiles usr/lib/tmpfiles.d/obarun.conf
}
