pkg/mkinitcpio/usr/lib/initcpio/install/lvm2:    # this udev rule is specific for systemd and non-systemd systems
pkg/mkinitcpio/usr/lib/initcpio/install/lvm2:    if declare -F add_systemd_unit &>/dev/null; then
pkg/mkinitcpio/usr/lib/initcpio/install/mdadm_udev:    if declare -F add_systemd_unit &>/dev/null; then
pkg/mkinitcpio/usr/lib/initcpio/install/mdadm_udev:        add_systemd_unit 'mdmon@.service'
pkg/mkinitcpio/usr/lib/initcpio/functions:    # add os-release and initrd-release for systemd
pkg/mkinitcpio/usr/lib/initcpio/init_functions:        # ensure that root is going to be mounted rw. Otherwise, systemd
pkg/mkinitcpio/usr/lib/initcpio/init_functions:# TODO: this really needs to follow the logic of systemd's encode_devnode_name
pkg/mkinitcpio/usr/share/mkinitcpio/hook.preset:#default_options="--splash /usr/share/systemd/bootctl/splash-arch.bmp"
pkg/mkinitcpio/usr/bin/mkinitcpio:        for stub in /usr/lib/{systemd/boot/efi,gummiboot}/"linux${uefiarch}.efi.stub"; do

