opentmpfiles is a  utility script written in pure POSIX sh to parse and apply tmpfiles.d style file coming from systemd.
This script is a modified copy of `https://github.com/OpenRC/opentmpfiles`.

License
---

BSD 2-Clause

Contact information
---

* Email:
  Eric Vidal `<eric@obarun.org>`

* Web site:
  https://web.obarun.org/

* Forum:
  https://forum.obarun.org/

* XMPP Channel:
  obarun@muc.syntazia.org

Supports the project
---

Please consider to make [donation](https://web.obarun.org/index.php?id=18)
