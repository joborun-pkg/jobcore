#!/usr/bin/bash
# JOBoRun		: Jwm OpenBox Obarun RUNit
# Maintainer	: Joe Bo Run <joborun@disroot.org>
# PkgSource		: url="https://git.disroot.org/joborun-pkg/jobcore/src/branch/main/$pkgname"
# Website		: https://pozol.eu
#-----------------------------------------| DESCRIPTION |---------------------------------------

pkgbase=nfs-utils
pkgdesc="Support programs for Network File Systems - w/o ipv6 gssproxy & systemd "
pkgname=('nfs-utils' 'nfsidmap')
pkgver=2.8.2
pkgrel=02
url='http://nfs.sourceforge.net'
makedepends=('libevent' 'sqlite' 'rpcsvc-proto' 'device-mapper' 'libnl')
# http://git.linux-nfs.org/?p=steved/nfs-utils.git;a=summary
#options=('debug')  ###  uncomment this to have the debug pkg produced
source=(https://www.kernel.org/pub/linux/utils/${pkgname}/${pkgver}/${pkgname}-${pkgver}.tar.{xz,sign}
        exports
        sysusers.d
        sha256sums::https://mirrors.edge.kernel.org/pub/linux/utils/nfs-utils/$pkgver/sha256sums.asc
        0001-fix-nfsidmap-missing-functions.patch)

##
## Note from Joborun: gssproxy since 0.9.0 has become next to impossible to build without systemd-aemon
## So since this is the only pkg dependent on it we opted in rebuilding nfs-utils without gssproxy
##

prepare() {
  cd "${pkgbase}"-${pkgver}
 
  # fix user id mapping - #2
  patch -Np1 -i ../0001-fix-nfsidmap-missing-functions.patch

  # fix hardcoded sbin/libexec path to our needs
  sed -i "s|sbindir = /sbin|sbindir = /usr/bin|g" utils/*/Makefile.am
  sed -i "s|sbin|bin|" utils/nfsidmap/id_resolver.conf
  sed -i "s|libexec|bin|" tools/nfsrahead/99-nfs.rules
  autoreconf -vfi
}

build() {
  cd "${pkgbase}"-${pkgver}
  ./configure --prefix=/usr \
    --sbindir=/usr/bin \
    --sysconfdir=/etc \
    --libexecdir=/usr/bin \
    --with-statedir=/var/lib/nfs \
    --with-statdpath=/var/lib/nfs/statd \
    --with-start-statd=/usr/bin/start-statd \
    --enable-nfsv4server \
    --disable-gss \
    --without-tcp-wrappers \
    --disable-ipv6 \
    --enable-libmount-mount \
    --enable-mountconfig \
    --without-systemd
  sed -i -e 's/ -shared / -Wl,-O1,--as-needed\0/g' libtool
  make
}

check() {
  cd "${pkgbase}"-${pkgver}
#  make -k check || /bin/true
  make check
}

package_nfs-utils() {

  pkgdesc="Support programs for Network File Systems - w/o ipv6 & systemd"

  backup=(etc/{exports,nfs.conf,nfsmount.conf}
          var/lib/nfs/{etab,rmtab})
  depends=('rpcbind' 'nfsidmap' 'gssproxy' 'libevent' 'device-mapper'
           'libxml2' 'libtirpc' 'e2fsprogs' 'keyutils' 'util-linux-libs'
           'libnl' 'krb5' 'libcap' 'gcc-libs' 'glibc' 'sh' 'readline')
  optdepends=('sqlite: for nfsdcltrack and fsidd usage'
              'python: for rpcctl, nfsiostat, nfsdclnts and mountstats usage')

  cd "${pkgbase}"-${pkgver}
  make DESTDIR="$pkgdir" install
 
  install -D -m 644 utils/mount/nfsmount.conf "$pkgdir"/etc/nfsmount.conf
  install -D -m 644 nfs.conf "$pkgdir"/etc/nfs.conf
  
  install -d -m 755 "$pkgdir"/usr/share/doc/$pkgname
  
  # docs
  install -m 644 {NEWS,README} "$pkgdir"/usr/share/doc/$pkgname/

  # empty exports file  
  install -D -m 644 ../exports "$pkgdir"/etc/exports

  # config file for idmappers in newer kernels
  install -D -m 644 utils/nfsidmap/id_resolver.conf "$pkgdir"/etc/request-key.d/id_resolver.conf

  mkdir "$pkgdir"/etc/exports.d
  mkdir -m 555 "$pkgdir"/var/lib/nfs/rpc_pipefs
  mkdir "$pkgdir"/var/lib/nfs/v4recovery

  # systemd sysusers - FS#75536
  install -D -m 644 ../sysusers.d "${pkgdir}"/usr/lib/sysusers.d/rpcuser.conf
  chown -Rv 34:34 "${pkgdir}"/var/lib/nfs/statd
  chmod -R 700 "${pkgdir}"/var/lib/nfs/statd
  chmod 644 "${pkgdir}"/var/lib/nfs/statd/state

  # nfsidmap cleanup
  rm -vrf "$pkgdir"/usr/include #/nfsid*
  rm -vrf "$pkgdir"/usr/lib/libnfsidmap*
  rm -vrf "$pkgdir"/usr/lib/pkgconfig #/libnfsidmap.pc
  rm -v "$pkgdir"/usr/share/man/{man3/nfs4_uid_to_name*,man5/idmapd.conf*}
  rm -vrf "$pkgdir"/usr/share/man/man3
}

package_nfsidmap() {

  pkgdesc="Library to help mapping IDs, mainly for NFSv4 - w/o ipv6 & systemd"
  backup=(etc/idmapd.conf)
  depends=('libldap' 'krb5' 'glibc')
 
  cd "${pkgbase}"-${pkgver}
  make -C support  DESTDIR="$pkgdir" install
  
  # part of -utils pkg
  rm -v "$pkgdir"/usr/bin/fsidd
  rmdir -v "$pkgdir"/usr/bin

  # config file  
  install -D -m 644 support/nfsidmap/idmapd.conf "$pkgdir"/etc/idmapd.conf
  # license
  install -Dm644 support/nfsidmap/COPYING $pkgdir/usr/share/licenses/nfsidmap/LICENSE
}

#---- arch license gpg-key & sha256sums ----

arch=(x86_64)

license=('GPL-2.0-or-later')

validpgpkeys=('E1B71E339E20A10A676F7CB69AFB1D681A125177') # Steve Dickson <steved@redhat.com>

## 885c948a84a58bca4148f459588f9a7369dbb40dcc466f04e455c6b10fd0aa48  nfs-utils-2.7.1.tar.xz

# https://mirrors.edge.kernel.org/pub/linux/utils/nfs-utils/2.8.2/sha256sums.asc
# c9d99d0c797035570fee0dc153773ad27fded677069068ced3b76be4dcb64915  nfs-utils-2.8.2.tar.gz
# a39bbea76ac0ab9e6e8699caf3c308b6b310c20d458e8fa8606196d358e7fb15  nfs-utils-2.8.2.tar.xz

sha256sums=(a39bbea76ac0ab9e6e8699caf3c308b6b310c20d458e8fa8606196d358e7fb15  # nfs-utils-2.8.2.tar.xz
	7b2f6f27532824992151db6ba9bc90909df3a864f29ce4128df3533a46c379ee  # nfs-utils-2.8.2.tar.sign
	b8238b74179f7e1626db2b637671ddc17288a1c5b7692954ae6d2fbb1be3788d  # exports
	61a3fbe55e30bc9c5da17a76e772c8be1cc695e0142a144c15f0f9aa46ab9ce0  # sysusers.d
	5bcbae77f58f5bb000dbd957bd531f7d6219834d5d0c8958d8451531035be15c  # sha256sums
	74a9c27fa82e4d4411d99ed65712363467dabcb17c76f9a9ae9d7c3ebb50994e) # 0001-fix-nfsidmap-missing-functions.patch

##  1ad42bcd97a3d430748bbe1f32fa3f65e65a09962038d4f1a33727feff8da9bb  nfsidmap-2.8.2-02-x86_64.pkg.tar.lz
##  1ac85cad76f167155a352189c927627fc8996859ad6c31c639d8c9db8729a2cf  nfs-utils-2.8.2-02-x86_64.pkg.tar.lz

